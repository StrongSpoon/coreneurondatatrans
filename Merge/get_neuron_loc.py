from snudda.utils.load import SnuddaLoad
from snudda.neurons.neuron_model_extended import NeuronModel
from snudda.utils.snudda_path import snudda_parse_path
from snudda.simulate.nrn_simulator_parallel import NrnSimulatorParallel
import numpy as np
import json
import tqdm

root_dir = "/mnt/lizhixin/networks/shapetest/100VoltTest/"
network_file = root_dir + "network-synapses.hdf5"
snudda_loader = SnuddaLoad(network_file)
network_info = snudda_loader.data

neurons = {}
config_file = root_dir + "network-config.json"#snudda_parse_path(network_info["configFile"])
CONFIG = None
with open(config_file, 'r') as config_file:
    CONFIG = json.load(config_file)

# read neuron name
name_pos_dic = {}
sim = NrnSimulatorParallel(cvode_active=False, dt=0.1)
for neuron_info in network_info["neurons"]:
    neuron_name = neuron_info["name"]
    neuron_id = neuron_info["neuronID"]

    name = network_info["neurons"][neuron_id]["name"]
    config = CONFIG["Neurons"][name]
    morph = snudda_parse_path(config["morphology"])
    param = snudda_parse_path(config["parameters"])
    mech = snudda_parse_path(config["mechanisms"])
    if "modulation" in config:
        modulation = snudda_parse_path(config["modulation"])
    else:
        modulation = None
    parameter_id = network_info["neurons"][neuron_id]["parameterID"]
    modulation_id = network_info["neurons"][neuron_id]["modulationID"]

    '''
    neurons[neuron_id] = NeuronModel(param_file=param,
                                     morph_file=morph,
                                     mech_file=mech,
                                     cell_name=name,
                                     modulation_file=modulation,
                                     parameter_id=parameter_id,
                                     modulation_id=modulation_id)
    neurons[neuron_id].instantiate(sim=sim)
    '''
    name_pos_dic[str(neuron_id)] = [neuron_info['position'], neuron_info['rotation']]
    #print(str(neurons[neuron_id].icell), neuron_info['position'], neuron_info['rotation'])

    #neuron_position = neuron_info['position']
    #neuron_rotation = neuron_info['rotation']
    # print(neuron_name, neuron_id)
    # print(neuron_position)
    # print(neuron_rotation)

print("length of name_pos_dic", len(name_pos_dic))

# rewrite file
file_in = root_dir + "nrn-output/vertex-dst"
file_out = root_dir + "nrn-output/vertex"
result = ""
point_info = []
with open(file_out, "w"):
    pass
for line in open(file_in, "r"):
    lines = line.replace("\n", "").split(" ")
    x, y, z, d, segid, neuronid = lines
    x, y, z, d = float(x), float(y), float(z), float(d)
    position, rotation = name_pos_dic[neuronid]
    position = position * 1000000
    #print(position)
    x,y,z = np.transpose(np.matmul(rotation, np.transpose([x,y,z])))
    x,y,z = [x,y,z] + position
    point_info.append("%f %f %f %f %s\n"%(x, y, z, d, segid))
    if len(point_info) > 5000:
        with open(file_out, "a") as f:
            f.writelines(point_info)
            point_info = []
if len(point_info) > 0:
    with open(file_out, "a") as f:
        f.writelines(point_info)
