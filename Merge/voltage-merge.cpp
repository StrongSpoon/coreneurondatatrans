#include <stdlib.h>
#include <cstdio>
#include <string>

using namespace std;

void mergefile(FILE* &src_fp, int arraySize, FILE* &dst_fp) {
	float* volt_buffer = new float[640000];
	fread(volt_buffer, sizeof(float), arraySize, src_fp);
	fwrite(volt_buffer, sizeof(float), arraySize, dst_fp);
	printf("VOLTAGE = %f \n", volt_buffer[0]);
	return;
}

int main(int argc, char** argv) {
	string root_dir = "/mnt/lizhixin/networks/shapetest/100VoltTest/records/";
	int proc = 1;

	if (argc > 1) {
		root_dir = argv[1];
		proc = atoi(argv[2]);
	}
	
	string src_file = root_dir + "rec_voltages";
	string dst_file = root_dir + "rec_voltages";

	FILE** src_fp = new FILE*[80];
	FILE* dst_fp = fopen(dst_file.c_str(), "wb");
	for (int i = 0; i < proc; i++) {
		src_fp[i] = fopen((src_file + to_string(i)).c_str(), "r");
	}

	int arraySize[80];
	int totalSize = 0, snapshotSize, tmpbuf, tmp1 = 1, tmp0 = 0;
	for (int i = 0; i < proc; i++) {
		printf("############ FILE %d ###############\n", i);
		fscanf(src_fp[i], "%d %d %d %d\n", &(arraySize[i]), &snapshotSize, &tmpbuf, &tmpbuf);
		totalSize += arraySize[i];
	}

	// fprintf(dst_fp, "%d %d %d %d\n", totalSize, snapshotSize, tmpbuf, tmpbuf);
	fwrite(&totalSize, sizeof(int), 1, dst_fp);
	fwrite(&snapshotSize, sizeof(int), 1, dst_fp);
	fwrite(&tmp1, sizeof(int), 1, dst_fp);
	fwrite(&tmp0, sizeof(int), 1, dst_fp);
	printf("%d %d\n", totalSize, snapshotSize);

	for (int i = 0; i < snapshotSize; i++) {
		printf("=========== TIMESTEP %d ===========\n", i);
		for (int j = 0; j < proc; j++) {
			mergefile(src_fp[j], arraySize[j], dst_fp);
		}
	}

	for (int i = 0; i < proc; i++) {
		fclose(src_fp[i]);
	}

	fclose(dst_fp);
}

