#include <stdlib.h>
#include <cstdio>
#include <string>

using namespace std;

void accuSegment(int* SegCount, string src_dir, int procs) {
	FILE* fp;
	for (int i = 0; i < procs; i++) {
		fp = fopen((src_dir + "records/voltage-dst" + to_string(i) + ".rec").c_str(), "rb");
		int segSize;
		fscanf(fp, "%d", &segSize);
		SegCount[i + 1] = SegCount[i] + segSize;
		printf("---------- %d ----------\n", segSize);
		fclose(fp);
	}
	return;
}

int main(int argc, char** argv) {
	int SegCount[100] = {0};

	string src_dir = "/mnt/lizhixin/networks/shapetest/10000Right/";
	int procs = 10;

	if (argc > 1) {
		src_dir = argv[1];
		procs = atoi(argv[2]);
	}

	accuSegment(SegCount, src_dir, procs);
	string dst_file = src_dir + "output/vertex-dst";
	string line_file = src_dir + "output/VertexCounter";
	string src_file = src_dir + "output/vertex";

	FILE* dst_fp = fopen(dst_file.c_str(), "wb");
	FILE* line_fp = fopen(line_file.c_str(), "wb");
	FILE* src_fp;
	int lineCount = 0;
	fprintf(line_fp, "%d\n", lineCount);
	for (int i = 0; i < procs; i++) {
		printf("============== VERTEX %d =============\n", i);
		src_fp = fopen((src_file + to_string(i)).c_str(), "r");
		float x, y, z, d;
		int segID, nrnID;
		while(fscanf(src_fp, "%f %f %f %f %d %d\n", &x, &y, &z, &d, &segID, &nrnID) > 0) {
			segID += SegCount[i];
			nrnID = nrnID * procs + i;
			fprintf(dst_fp, "%f %f %f %f %d %d\n", x, y, z, d, segID, nrnID);
			lineCount ++;
		}
		printf("############# %d LINE #############\n", lineCount);
		fprintf(line_fp, "%d\n", lineCount);
		fclose(src_fp);
	}

	fclose(dst_fp);
	fclose(line_fp);
}
