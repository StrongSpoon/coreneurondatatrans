import numpy as np
from tqdm import tqdm
import struct

root_dir = "/mnt/lizhixin/networks/shapetest/10000D/snudda-output/"
loc_file = root_dir + "3d_point_location_trans.txt"
voltage_file = root_dir + "volt.txt"
save_vertex_file = root_dir + "vertex"
save_indices_file = root_dir + "indices"
save_voltage_file = root_dir + "rec_voltages"

# read 3d loc file and construct vertex id dic and seg id dic
vertex_id_doc = {}  # (neuron id, vec local id) -> vec global id
seg_id_doc = {}     # (neuron id, sec id, seg local id) -> seg global id
pre_neuron_id = '-1'
vertex_local_id = -1
vertex_global_id = -1
seg_global_id = -1


with open(save_vertex_file, 'w'):
    pass
with open(save_indices_file, 'w'):
    pass


for line in tqdm(open(loc_file, "r")):
    x, y, z, d, parent_id, seg_id, sec_id, neuron_id, _ = line.replace("\n","").split(" ")

    if pre_neuron_id != neuron_id:
        pre_neuron_id = neuron_id
        vertex_local_id = -1

    vertex_local_id += 1
    vertex_global_id += 1
    vertex_id_doc[(neuron_id, str(vertex_local_id))] = vertex_global_id

    if seg_id_doc.get((neuron_id, sec_id, seg_id), -1) == -1:
        seg_global_id += 1
        seg_id_doc[(neuron_id, sec_id, seg_id)] = seg_global_id
    
    with open(save_vertex_file, 'a') as f_vertex:
        f_vertex.writelines("%f %f %f %f %d\n"%(float(x), float(y), float(z), float(d), seg_id_doc.get((neuron_id, sec_id, seg_id))))
    
    if parent_id != "-1":
        with open(save_indices_file, 'a') as f_indices:
            parent_global_id = vertex_id_doc[(neuron_id, parent_id)]
            f_indices.writelines("%d %d\n"%(parent_global_id, vertex_global_id))

print("vertex_global_id: ", vertex_global_id)
print("seg_global_id: ", seg_global_id)


nparam = seg_global_id + 1
with open(voltage_file, "r") as f:
    volt = f.readline().replace("\n", "").split(',')
rec_len = len(volt)-5
print("rec_len", rec_len)
voltage = np.zeros((rec_len, nparam), dtype=np.float32)
c = 0
a = 0
minv = 100
maxv = -100
for line in tqdm(open(voltage_file, "r")):
    volt = line.replace("\n", "").split(',')
    neuron_id, sec_id, seg_id = volt[0], volt[1], volt[2]
    c += 1
    if seg_id_doc.get((neuron_id, sec_id, seg_id), -1) == -1:
        a += 1
        continue
    sid = seg_id_doc[(neuron_id, sec_id, seg_id)]
    for t in range(rec_len):
        voltage[t][sid] = volt[t+3]

for i, v in enumerate(voltage):
    for j in v:
        if j > -1 and j < 1:
            print(i, v)
        if minv > j:
            minv = j
        if maxv < j:
            maxv = j
print("min, max:", minv, maxv)


voltage = voltage.astype('float32')
print(nparam, rec_len, 1, 0)
with open(save_voltage_file, 'wb') as f_volt:
    #s = "%d %d 1 0\n"%(nparam, rec_len)
    #f_volt.writelines(s)
    f_volt.write(struct.pack("iiii", nparam, rec_len, 1, 0))  
    f_volt.write(voltage.tobytes())
#with open(save_voltage_file, 'ab') as f_volt:
print("voltage_num", c)
