# Transform for Visualization by Mengdi Zhao

Snudda outputs 3d point locations (after updating offsets of neurons) and voltages.

Code trans_for_visual.py generates file vertex, indices, and rec_voltages in the wanted type.

You could set the root_dir (in line 5) to your network directory and run it.

# Voltage Merge by Zhixin Li

File voltage-merge.cpp merges voltage files from all processes into one.

You can compile it with g++ and run it using volt.sh, in which root_dir is the directory of voltage files and procs is the number of processes.

# Segment Merge by Zhixin Li

File segment-merge.cpp merges vertex files from all processes into one.

You can compile it with g++ and run it using segment.sh, in which root_dir is the directory of network and procs is the number of processes.

# Indices Merge by Zhixin Li

File indices-merge.cpp merges indices file from all processes into one.

You can compile it with g++ and run it using indices.sh, in which root_dir is the directory of indices files and procs is the number of processes.

# Get Neuron Location by Mengdi Zhao

Add coordinary offsets of each neuron to vertex file.

You could set the root_dir (int line 9) to your network directory and set output directories in line 61 and line 62.