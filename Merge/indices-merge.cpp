#include <stdlib.h>
#include <cstdio>
#include <string>

using namespace std;

int main(int argc, char** argv) {
	string root_dir = "/mnt/lizhixin/networks/shapetest/10000Right/nrn-output/";
	int procs = 10;

	if (argc > 1) {
		root_dir = argv[1];
		procs = atoi(argv[2]);
	}

	string src_file = root_dir + "indices";
	string dst_file = root_dir + "indices-dst";
	string line_file = root_dir + "VertexCounter";

	FILE* dst_fp = fopen(dst_file.c_str(), "wb");
	FILE* line_fp = fopen(line_file.c_str(), "r");
	FILE* src_fp;

	for (int i = 0; i < procs; i++) {
		printf("=============== INDICES %d ==============\n", i);
		src_fp = fopen((src_file + to_string(i)).c_str(), "r");
		int indice1, indice2;
		int offset;
		fscanf(line_fp, "%d\n", &offset);
		while(fscanf(src_fp, "%d %d\n", &indice1, &indice2) > 0) {
			indice1 += offset;
			indice2 += offset;
			fprintf(dst_fp, "%d %d\n", indice1, indice2);
		}
		fclose(src_fp);
	}
	fclose(dst_fp);
	fclose(line_fp);
	return 0;
}
