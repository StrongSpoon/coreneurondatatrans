# Node Transformer by Yichen Zhang

Node-Transformer is used to transform node and section files from Neuron into vertex and indice files for visualization.

The original code files are in https://gitee.com/HolyLow/node-transformer .

Clone the project from the above address and replace src.cpp with the file of the same name here.

First run build.sh to build it and generate src.

Then you can run it by loop.sh.

In loop.sh:

    procs -- the number of processes in parallelization

    ./src -- the executable file

    /mnt/lizhixin/networks/shapetest/100VoltTest/output/ -- the directory in which your node and section files are

    100 -- the number of cells in each process
