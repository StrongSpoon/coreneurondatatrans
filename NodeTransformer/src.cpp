#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "glad/glad.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include <iostream>
#include <vector>
#include "neuron.h"
#include "tnode.h"
// #include <Windows.h>
#include <dlfcn.h>
#include "time.h"

#define RANDMAX 999

using namespace std;

int read_color_map(const char* filename, float* &colormap);
int read_rec_file(const char* filename, float** &rec_vals, int &nparam);
void read_comp_file(const char* filename, unsigned short* &comp_vals, unsigned short* &current_stages,
	unsigned short* &nums, int* &offsets, int &nparam, int &len);
//int read_color_stages(const char* filename, float** &color_stages, int &nparam);


int read_data_points(const char* section_file, const char* node_file, Section*& sec_arr, Node*& node_arr, int &nsection, int ncell)
{
	int nnode, has_logical;
	FILE *fp;
	fp = fopen(section_file, "rb");
	fread(&nsection, sizeof(int), 1, fp);
	sec_arr = new Section[nsection];
	for (int i = 0; i < nsection; i++)
	{
		fread(&sec_arr[i].order, sizeof(int), 1, fp);
		fread(&sec_arr[i].icell, sizeof(int), 1, fp);
		//sec_arr[i].icell = 0;
		fread(&sec_arr[i].par_order, sizeof(int), 1, fp);
		fread(&has_logical, sizeof(int), 1, fp);
		//printf("isec:%d\n", i);
		if (has_logical)
		{
			sec_arr[i].logical_connection = new float[3];
			fread(sec_arr[i].logical_connection, sizeof(float), 3, fp);
		}
		fread(&sec_arr[i].nnode, sizeof(short), 1, fp);
		fread(&sec_arr[i].npt3d, sizeof(short), 1, fp);
		sec_arr[i].points3d = new float[3 * sec_arr[i].npt3d];
		sec_arr[i].colors = new float[3 * sec_arr[i].npt3d];
		sec_arr[i].diam = new float[sec_arr[i].npt3d];
		for (int j = 0; j < sec_arr[i].npt3d; j++) 
		{
			fread(sec_arr[i].points3d + 3 * j, sizeof(float), 3, fp);
			fread(sec_arr[i].diam + j, sizeof(float), 1, fp);
		}
	}
	fclose(fp);

	fp = fopen(node_file, "rb");
	fread(&nnode, sizeof(int), 1, fp);
	node_arr = new Node[nnode];
	for (int i = 0; i < nnode; i++)
	{
		fread(&node_arr[i].index, sizeof(int), 1, fp);
		int temp;
		temp = node_arr[i].index;
		//printf("%d\n", node_arr[i].index);
		node_arr[i].index = i;
		//if (temp != node_arr[i].index)
		//	printf("error %d %d\n", temp, node_arr[i].index);
		fread(&node_arr[i].sec_order, sizeof(int), 1, fp);
		fread(&node_arr[i].index_in_sec, sizeof(int), 1, fp);
		fread(&node_arr[i].par_order, sizeof(int), 1, fp);
		//printf("node:%d secid:%d cellid:%d\n", i, node_arr[i].sec_order, sec_arr[node_arr[i].sec_order].icell);
		if (i >= ncell)
			sec_arr[node_arr[i].sec_order].nodes.push_back(&node_arr[i]);
	}

	for (int i = 0; i < nsection; i++)
		sec_arr[i].nnode = sec_arr[i].nodes.size();
	fclose(fp);

	return nnode;
}

int gen_vertex_array(float*& vertices, unsigned int* &indices, int &nindices, std::vector<Neuron*> neuron_vec,  
					int stride, int nnode, int nparam, int* map_spine2node, int* spine_obj, Node* node_arr, 
					Section* sec_arr, unsigned int ntex_row, unsigned int ntex_col,
					const char* vertex_file, const char* indice_file)
{
	int ncell = neuron_vec.size();
	int nvertices = 0;
	for (int i = 0; i < ncell; i++)
	{
		nvertices += neuron_vec[i]->npoints;
		nindices += neuron_vec[i]->nindices;
	}

	vertices = new float[nvertices * stride];
	float* all_diam = new float[nvertices];
	int *nodeid_arr = new int[nvertices];
	int *cell_id_arr = new int[nvertices];
	indices = new unsigned int[nindices];
	int ivertex = 0, iindex = 0;
	//int offsets = 3 * nvertices;
	for (int icell = 0; icell < ncell; icell++)
	{
		Neuron* neuron = neuron_vec[icell];
		for (int isec = 0, nsec = neuron->all_secs.size(); isec < nsec; isec++)
		{
			neuron->all_secs[isec]->global_start_pos += ivertex;
			neuron->all_secs[isec]->global_end_pos += ivertex;
		}

		for (int idx = 0; idx < neuron->nindices; idx++)
		{
			indices[iindex] = neuron->draw_indices[idx] + ivertex;
			iindex++;
		}

		for (int ipoint = 0; ipoint < neuron->npoints; ipoint++)
		{
			vertices[ivertex * stride + 0] = neuron->points3d[ipoint * 3 + 0];
			vertices[ivertex * stride + 1] = neuron->points3d[ipoint * 3 + 1];
			vertices[ivertex * stride + 2] = neuron->points3d[ipoint * 3 + 2];

			vertices[ivertex * stride + 3] = 0.0f;
			vertices[ivertex * stride + 4] = 0.0f;
			vertices[ivertex * stride + 5] = 0.0f;

			//for (int i = 0; i < stride - 6; i++)
			//	vertices[ivertex * stride + i] = 0.0f;

			//float pos = ivertex * 1.0f / nvertices;
			//pos = 1.0f - pos;
			vertices[ivertex * stride + 6] = -1.0f; 
			vertices[ivertex * stride + 7] = 0.0f;

			all_diam[ivertex] = neuron->diam_arr[ipoint];
			cell_id_arr[ivertex] = icell;

			ivertex++;
		}
	}

	int start, end, sec_id;

	for (int icell = 0; icell < ncell; icell++)
	{
		Neuron* neuron = neuron_vec[icell];
		for (int isec = 0, nsec = neuron->all_secs.size(); isec < nsec; isec++)
		{
			Section* sec = neuron->all_secs[isec];
			int sec_nvertex = sec->global_end_pos - sec->global_start_pos;
			int npt_per_node = sec_nvertex / sec->nnode;
			for (int inode = 0; inode < sec->nnode; inode++)
			{
				Node* nd = sec->nodes[inode];
				start = sec->global_start_pos + npt_per_node * inode;
				end = start + npt_per_node;
				if (inode == sec->nnode - 1)
					end = sec->global_end_pos;
				//if (nd->index == 3000)
				//	printf("hit\n");
				//if (icell == 0)
				//	printf("sec:%d node:%d pos:%f start:%d end:%d\n", sec->order, nd->index, (nd->index * 1.0f - ncell) / (nnode - ncell), start, end);
				for (int i = start; i < end; i++)
				{
					unsigned int row, col;
					row = (nd->index - ncell) / ntex_col;
					col = (nd->index - ncell) % ntex_col;
					//vertices[i * stride + 6] = (col * 1.0f) / (ntex_col - 1);
					//vertices[i * stride + 7] = (row * 1.0f) / (ntex_row - 1);
					nodeid_arr[i] = nd->index - ncell;
					//vertices[i * stride + 6] = (nd->index - ncell) * 1.0f;
					//vertices[i * stride + 6] = 0.0f;
				}
			}

		}
	}

	int *parent = NULL;
	parent = new int[nvertices];
	for (int i = 0; i < nvertices; i++)
		parent[i] = -1;
	for (int i = 0, len = nindices / 2; i < len; i++)
		parent[indices[i * 2 + 1]] = indices[i * 2];

	FILE *fp;
	fp = fopen(vertex_file, "w");
	for (int ivertex = 0; ivertex < nvertices; ivertex++)
	{
		//if (nodeid_arr[ivertex] < ncell)
		//	printf("%d %d\n", ivertex, nodeid_arr[ivertex]);
		fprintf(fp, "%f %f %f %f %d %d\n", vertices[ivertex * stride + 0], vertices[ivertex * stride + 1], vertices[ivertex * stride + 2], all_diam[ivertex], nodeid_arr[ivertex], cell_id_arr[ivertex]);
		//fprintf(fp, "%f %f %f %f %d %d\n", vertices[ivertex * stride + 0], vertices[ivertex * stride + 1], vertices[ivertex * stride + 2], all_diam[ivertex], parent[ivertex], nodeid_arr[ivertex]);
	}
	fclose(fp);

	delete[] all_diam;
	delete[] nodeid_arr;
	delete[] cell_id_arr;
	if (parent)
		delete[] parent;

	fp = fopen(indice_file, "w");
	for (int i = 0, len = nindices / 2; i < len; i++)
	{
		fprintf(fp, "%d %d\n", indices[i * 2], indices[i * 2 + 1]);
	}
	fclose(fp);
	
	return nvertices;
}

void points_transform(float* points, int npoint, glm::mat4 mat)
{
	for (int i = 0; i < npoint; i++)
	{
		glm::vec4 p(points[i * 3 + 0], points[i * 3 + 1], points[i * 3 + 2], 1);
		glm::vec4 p2;
		p2 = mat * p;
		points[i * 3 + 0] = p2.x;
		points[i * 3 + 1] = p2.y;
		points[i * 3 + 2] = p2.z;
		//printf("(%f %f %f) -> (%f %f %f)\n", p.x, p.y, p.z, p2.x, p2.y, p2.z);
	}
}

void gen_network(std::vector<Neuron*> &neuron_vec, int ncell, int ncluster, Section* sec_arr, int nsec)
{
	int ncell_per_cluster = ncell / ncluster;
	vector<vector<Section*> > sec_vec;
	sec_vec.resize(ncell);
	for (int isec = 0; isec < nsec; isec++)
	{
		int icell = sec_arr[isec].icell;
		if (icell >= ncell)
			continue;
		sec_vec[icell].push_back(&sec_arr[isec]);
	}
	for (int icell = 0; icell < ncell; icell++)
	{
		Neuron* neuron = new Neuron(icell, sec_vec[icell]);
		//neuron->set_sections(sec_vec[icell]);
		//neuron->set_points(sec_arr);
		neuron_vec.push_back(neuron);
	}

	float cluster_centers[6][3] = { 0.0f, 0.0f, 0.0f,
	-27.5f, -20.2f, -50.0f,
	0.0f, 20.2f, -50.0f,
	0.0f, -20.2f, -50.0f,
	27.5f, 20.2f, -50.0f,
	25.5f, -20.6f, -50.0f };

	srand(0);
	float x_max = -999.0, x_min = 9999.0, y_max = -9999.0, y_min = 9999.0, z_max = -9999.0, z_min = 9999.0;
	float max_abs = 0.0f;
	float scale = 0.9 / max_abs;
	scale = 1000;
	int npos;
	//printf("xmin:%f xmax:%f\nymin:%f ymax:%f\nzmin:%f zmax:%f\n", x_min, x_max, y_min, y_max, z_min, z_max);
	//FILE *fp = fopen("../../striatum_50cell/positions", "r");
	//fscanf(fp, "%d", &npos);
	//assert(npos == ncell);
	for (int icluster = 0; icluster < ncluster; icluster++)
	{
		for (int icell = 0; icell < ncell_per_cluster; icell++)
		{
			Neuron* neuron = neuron_vec[icluster * ncell_per_cluster + icell];
			glm::mat4 model(1.0f);
			float pos[3];
			float rotat[3];
			/*for (int i = 0; i < 3; i++)
			{
				float offset = (rand() % RANDMAX * 1.0) / (RANDMAX + 1); //random number 0-1
				//offset = (offset - 0.5) * 30;
				//pos[i] = cluster_centers[icluster][i] + offset;
				//float temp = (rand() % RANDMAX * 1.0) / (RANDMAX + 1);
				//temp = (temp - 0.5) * 10;
				//rotat[i] = temp;
				if (i == 1)
					offset = (offset - 0.5) * 1000; //random number -5 -- 5
				else
					offset = (offset - 0.5) * 2500;
				pos[i] = cluster_centers[icluster][i] + offset;
				//fscanf(fp, "%f %f %f", &pos[0], &pos[1], &pos[2]);
				//printf("icell:%d %f %f %f\n", icell, pos[0], pos[1], pos[2]);
				//float offset = (rand() % RANDMAX * 1.0) / (RANDMAX + 1); //random number 0-1
				//offset = (offset - 0.5) * 20; //random number -5 -- 5
				//pos[i] = cluster_centers[icluster][i] + offset;
				//float temp = (rand() % RANDMAX * 1.0) / (RANDMAX + 1);
				//temp = (temp - 0.5) * 10;
				//rotat[i] = temp;
				//offset = 0;
			}*/
			//model = glm::translate(model, glm::vec3(pos[0], pos[1], pos[2]));
			//model = glm::scale(model, glm::vec3(0.04f, 0.04f, 0.04f));
			//model = glm::rotate(model, -3.05f, glm::vec3(1.0f, 0.0f, 0.0f));
			//model = glm::rotate(model, -9.40f, glm::vec3(0.0f, 1.0f, 0.0f));
			//model = glm::rotate(model, -0.27f, glm::vec3(0.0f, 0.0f, 1.0f));
			/*model = glm::rotate(model, rotat[0], glm::vec3(1.0f, 0.0f, 0.0f));
			model = glm::rotate(model, rotat[1], glm::vec3(0.0f, 1.0f, 0.0f));
			model = glm::rotate(model, rotat[2], glm::vec3(0.0f, 0.0f, 1.0f));*/
			//points_transform(neuron->points3d, neuron->npoints, model);
			/*for (int i = 0; i < neuron->npoints; i++)
			{
				neuron->diam_arr[i] *= 0.04f;
			}*/
		}
	}
	//fclose(fp);
}

int main(int argc, char** argv)
{

	//data process phase
	float *vertices = NULL;
	unsigned int* indices = NULL;
	Section *sec_arr = NULL;
	Node *node_arr = NULL;
	int nsection, nvertex, nindices = 0, ncluster = 1;
	string root_dir = "/mnt/lizhixin/networks/shapetest/10000Right/output/";
	string file_num = "0";
	int ncell = 2500;

	if (argc > 1) {
		root_dir = argv[1];
		file_num = argv[2];
		ncell = atoi(argv[3]);
	}

	std::vector<glm::vec3> all_vertex;
	int nnode = read_data_points((root_dir + "sections" + file_num).c_str(), (root_dir + "nodes" + file_num).c_str(), sec_arr, node_arr, nsection, ncell);
	printf("nsection:%d nnode:%d\n", nsection, nnode);


	std::vector<Neuron*> neuron_vec;
	//ncell = 300;
	int stride = 8;
	//ncell = 50;
	gen_network(neuron_vec, ncell, ncluster, sec_arr, nsection);
	//nvertex = gen_vertex_array(vertices, sec_arr, nsection);

	//texture
	float **rec_vals = NULL, *color_map = NULL;
	float* tex_data = NULL;
	//int ncolor_stage = read_color_map("jet.cm", color_map);
	unsigned short *comp_vals = NULL, *current_stages = NULL, *nums = NULL;
	int *offsets = NULL;
	int nparam = 0, len, nstep = 20000;
	int *map_spine2node = NULL, *spine_obj = NULL;
	FILE* fp = NULL;
	//read_comp_file((root_dir + "comp_vals").c_str(), comp_vals, current_stages, nums, offsets, nparam, len);
	/*len = read_rec_file((root_dir + "rec_vals").c_str(), rec_vals, nparam);
	fp = fopen("rec_voltages", "w");
	fprintf(fp, "%d %d\n", nparam, 4000);
	for (int iparam = 0; iparam < nparam; iparam++)
	{
		for (int istep = 10000; istep < 14000; istep++)
		{
			fprintf(fp, "%f ", rec_vals[iparam][istep]);
		}
		fprintf(fp, "\n");
	}
	fclose(fp);*/

	unsigned int ntex_col = 10000;
	//unsigned int ntex_row = nparam / ntex_col + 1;
	unsigned int ntex_row = 50;
	unsigned int ntex_layer = 2000;
	nvertex = gen_vertex_array(vertices, indices, nindices, neuron_vec, stride, nnode, nparam,
							   map_spine2node, spine_obj, node_arr, sec_arr, ntex_row, ntex_col,
							   (root_dir + "vertex" + file_num).c_str(), (root_dir + "indices" + file_num).c_str());
	if (rec_vals)
	{
		for (int i = 0; i < nparam; i++)
			delete[] rec_vals[i]; 
		delete[] rec_vals;
	}
	printf("%d\n", nvertex);

	for (int i = 0; i < ncell; i++)
		delete neuron_vec[i];
	if (tex_data)
	{
		delete[] tex_data;
	}
	if (sec_arr)
		delete[] sec_arr;
	if (node_arr)
		delete[] node_arr;
	if (vertices)
		delete[] vertices;
	if (color_map)
		delete[] color_map;
	if (comp_vals)
		delete[] comp_vals;
	if (current_stages)
		delete[] current_stages;
	if (nums)
		delete[] nums;
	if (offsets)
		delete[] offsets;
	if (indices)
		delete[] indices;
	if (map_spine2node)
		delete[] map_spine2node;
	if (spine_obj)
		delete[] spine_obj;
	return 0;
}
