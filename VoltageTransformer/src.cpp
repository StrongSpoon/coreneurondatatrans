#include <stdlib.h>
#include <string>
#include <cstdio>

using namespace std;

int main(int argc, char** argv) {
	string src_file;
	string src_dir;
	string src_num;
	if (argc > 1) {
		src_dir = argv[1];
		src_num = argv[2];
	}
	else {
		fprintf(stderr, "No Src File.\n");
		return 0;
	}
	int arraySize, snapshotSize, permuteFlag;
	float totalTime;
	src_file = src_dir + "voltage-seg" + src_num + ".rec";
	FILE* src_fp = fopen(src_file.c_str(), "r");
	printf("TRANSFORMING %s\n", src_file.c_str());
	fscanf(src_fp, "%d %f %d %d\n", &arraySize, &totalTime, &snapshotSize, &permuteFlag);
	printf("ArraySize:	%d\n", arraySize);
	printf("TotalTime:	%f\n", totalTime);
	printf("SnapshotSize:	%d\n", snapshotSize);
	printf("PermuteFlag:	%d\n", permuteFlag);
	// return 0;

	// ======================== FOR TEST ========================
	string test_file = src_dir + "voltage-test" + src_num + ".txt";
	FILE* test_fp = fopen(test_file.c_str(), "w");
	// ======================== FOR TEST ========================

	string dst_file = src_dir + "rec_voltages" + src_num;
	FILE* dst_fp = fopen(dst_file.c_str(), "wb");
	int default_value = 1;
	fprintf(dst_fp, "%d %d %d %d\n", arraySize, snapshotSize, default_value, default_value);

	int* shuffle = new int [650000];
	if (permuteFlag > 0) {
		fread(shuffle, sizeof(int), arraySize, src_fp);
	}

	// for (int i = 0; i < arraySize; i+=10000)
	// 	printf("########	%d\n", shuffle[i]);
	// return 0;

	double* tmpdouble = new double[650000];
	float* permuteArray = new float[650000];
	for (int i = 0; i < snapshotSize; i++) {
		printf("================= %d ==============\n", i);
	       	fread(tmpdouble, sizeof(double), arraySize, src_fp);
		// printf("=============== FLAG1 =============\n");
		for (int j = 0; j < arraySize; j++) {
			if (permuteFlag > 0 and false) {
				// printf("####### %d ########\n", shuffle[j]);
				permuteArray[j] = (float)(tmpdouble[shuffle[j]]);
				// permuteArray[shuffle[j]] = (float)(tmpdouble[j]);
			}
			else {
				permuteArray[j] = (float)tmpdouble[j];
			}
			
			// ======================== FOR TEST ========================
			fprintf(test_fp, "%f ", permuteArray[j]);
			// ======================== FOR TEST ========================

		}
		// printf("TRANS:		%f\n", permuteArray[1]);
		fwrite(permuteArray, sizeof(float), arraySize, dst_fp);
		
		// ======================== FOR TEST ========================
		fprintf(test_fp, "\n");
		// ======================== FOR TEST ========================
	}
	fclose(src_fp);
	fclose(dst_fp);
	fclose(test_fp);
	return 0;
}
