# Voltage Transformer by Zhixin Li

Voltage Transformer is a tool to transform voltage records into specific order.

You can use g++ to compile it and run by loop.sh.

In loop.sh:

    procs -- the number of processes in parallelization

    ./src -- the executable file you build
    
    /mnt/lizhixin/networks/shapetest/100VoltTest/records/ -- the directory in which your voltage record files are

DEFAULT CONFIGURE: We default that there are no more than 2500 neurons (approximately 65000 segments) in each process.
